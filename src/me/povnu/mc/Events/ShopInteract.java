package me.povnu.mc.Events;

import me.povnu.mc.interfaces.FoodShop;
import me.povnu.mc.utils.Color;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class ShopInteract implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        ItemStack i = e.getCurrentItem();

        if (e.getClickedInventory() == null) {
            return;
        }

        if (e.getClickedInventory().getTitle().equalsIgnoreCase("Coin Shop")) {
            e.setCancelled(true);
            if (i.getItemMeta().getDisplayName().equalsIgnoreCase(Color.color("&e&lFood Shop"))) {
                FoodShop.FoodUI(p);
                p.playSound(p.getLocation(), Sound.EAT, 1F, 1F);
                p.sendMessage(Color.color("&aOpening up the Food shop..."));
            }
        }
    }
}
