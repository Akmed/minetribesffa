package me.povnu.mc.commands;

import me.povnu.mc.items.CoinTypes;
import me.povnu.mc.utils.Color;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Coins implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player) sender;
        if ((args.length == 0) && (cmd.getName().equalsIgnoreCase("Coins"))) {
            p.sendMessage(Color.color("&e&lCoin Commands"));
            p.sendMessage(" ");
            p.sendMessage(Color.color("&7/Coins <give> <player> <amount> <type>"));

        }

        if ((args.length == 4) && (cmd.getName().equalsIgnoreCase("Coins"))) {
            if (!p.hasPermission("minetribesffa.give")) {
                p.sendMessage(Color.color("&cYou do not have permission to use this command."));
                return true;
            }
            if (args[0].equalsIgnoreCase("give")) {
                Player target = Bukkit.getPlayerExact(args[1]);
                if (target != null) {
                    int amount = Integer.parseInt(args[2]);
                    if ((amount != 0) && (amount >= 0)) {
                        if (args[3].equalsIgnoreCase("dark")) {
                            target.getInventory().addItem(new ItemStack[]{CoinTypes.darkCoin(amount)});
                        } else if (args[3].equalsIgnoreCase("gold")) {
                            target.getInventory().addItem(new ItemStack[]{CoinTypes.goldCoin(amount)});
                        } else if (args[3].equalsIgnoreCase("platinum")) {
                            target.getInventory().addItem(new ItemStack[]{CoinTypes.platCoin(amount)});
                        }
                    }
                } else {
                    p.sendMessage(Color.color("&cPlease check your command. Something is not correct make sure the player is online or valid."));
                }
            }
        }
        return false;
    }
}