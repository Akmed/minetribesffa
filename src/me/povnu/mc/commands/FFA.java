package me.povnu.mc.commands;


import me.povnu.mc.Main;
import me.povnu.mc.interfaces.Kit;
import me.povnu.mc.utils.Color;
import me.povnu.mc.utils.LocationsFile;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import org.bukkit.entity.Player;

public class FFA implements CommandExecutor {

    private LocationsFile locsFile;

    public FFA(LocationsFile locsFile) {
        this.locsFile = locsFile;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player) sender;

        if ((args.length == 0) && (cmd.getName().equalsIgnoreCase("ffa"))) {
            p.sendMessage(Color.color("&c&lFFA CommandsF"));
            p.sendMessage(" ");
            p.sendMessage(Color.color("&7/ffa setpoints - sets the spawn points for the ffa"));
            p.sendMessage(" ");
            p.sendMessage(Color.color("&7/ffa spawn -  teleports you to one of the random spawns"));
            p.sendMessage(" ");
            p.sendMessage(Color.color("&7/ffa npc - spawns the npc at your location."));
        }

        if ((args.length == 1) && (cmd.getName().equalsIgnoreCase("ffa"))) {
            if (args[0].equalsIgnoreCase("setpoints")) {
                locsFile.addSpawnLocation(p.getLocation());
                p.sendMessage(Color.color("&aYou have set a spawn point for the ffa "));
            }
        }

        if ((args.length == 1) && (cmd.getName().equalsIgnoreCase("ffa"))) {
            if (args[0].equalsIgnoreCase("kit")) {
                if (Main.getInstance().cooldown.containsPlayer(p.getPlayer())) {
                    p.sendMessage(Color.color("&c You cannot use the starter kit for another " + Main.getInstance().cooldown.getPlayer(p.getPlayer()) + " seconds!"));

                }
                if (!Main.getInstance().cooldown.containsPlayer(p.getPlayer())) {
                    Main.getInstance().cooldown.addPlayer(p, 5);
                    Main.getInstance().cooldown.runTimer(p);
                    Kit.useKit(p);
                }
            }
        }
        return false;
    }

}
