package me.povnu.mc.commands;

import me.povnu.mc.interfaces.StatsReset;
import me.povnu.mc.utils.Color;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class Stats implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player p = (Player) sender;

        if ((args.length == 0) && (cmd.getName().equalsIgnoreCase("stats"))) {
            p.sendMessage(Color.color("&8&m--------------------------"));
            p.sendMessage(Color.color("&7&l* &eName: &a" + p.getName()));
            p.sendMessage(Color.color("&7&l* &eKillStreak: &a{killstreak}"));
            p.sendMessage(Color.color("&7&l* &eKills: &a{Kills}"));
            p.sendMessage(Color.color("&7&l* &eDeaths: &a{deaths}"));
            p.sendMessage(Color.color("&7&l* &eK/D: &a{kd}"));
            p.sendMessage(Color.color("&8&m--------------------------"));

        }

        if ((args.length == 1) && (cmd.getName().equalsIgnoreCase("stats"))) {
            if (args[0].equalsIgnoreCase("reset")) {
                StatsReset.Reset((Player) sender);
            }
        }

        return false;
    }
}
