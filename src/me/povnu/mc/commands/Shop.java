package me.povnu.mc.commands;

import me.povnu.mc.interfaces.ShopGUI;
import me.povnu.mc.utils.Color;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Shop implements CommandExecutor {

    private ShopGUI ui;

    public Shop(ShopGUI ui) {
        this.ui = ui;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player) sender;

        if ((args.length == 0) && (cmd.getName().equalsIgnoreCase("shop"))) {
            ui.shopUI((Player) sender);
            p.sendMessage(Color.color("&aOpening the Coin shop..."));
            p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1F, 1F);
        }

        return false;
    }
}
