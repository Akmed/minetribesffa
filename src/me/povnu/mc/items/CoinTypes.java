package me.povnu.mc.items;

import me.povnu.mc.Main;
import me.povnu.mc.utils.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;


public class CoinTypes {

    public static ItemStack darkCoin(int amount) {
        ArrayList<String> lore = new ArrayList<>();

        ItemStack c = new ItemStack(Material.COAL);
        ItemMeta m = c.getItemMeta();

        m.setDisplayName(Color.color(Main.getInstance().getConfig().getString("Coins.Dark-Coin.Name")));

        for (String l : Main.getInstance().getConfig().getStringList("Coins.Dark-Coin.Lore")) {
            lore.add(Color.color(l));

        }

        m.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
        m.addItemFlags(ItemFlag.HIDE_ENCHANTS);

        m.setLore(lore);
        c.setAmount(amount);
        c.setItemMeta(m);
        return c;
    }

    public static ItemStack goldCoin(int amount) {
        ArrayList<String> lore = new ArrayList<>();

        ItemStack g = new ItemStack(Material.GOLD_NUGGET);
        ItemMeta m = g.getItemMeta();

        m.setDisplayName(Color.color(Main.getInstance().getConfig().getString("Coins.Gold-Coin.Name")));

        for (String l : Main.getInstance().getConfig().getStringList("Coins.Gold-Coin.Lore")) {
            lore.add(Color.color(l));

        }

        m.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
        m.addItemFlags(ItemFlag.HIDE_ENCHANTS);

        m.setLore(lore);
        g.setAmount(amount);
        g.setItemMeta(m);

        return g;
    }

    public static ItemStack platCoin(int amount) {
        ArrayList<String> lore = new ArrayList<>();

        ItemStack g = new ItemStack(Material.GHAST_TEAR);
        ItemMeta m = g.getItemMeta();

        m.setDisplayName(Color.color(Main.getInstance().getConfig().getString("Coins.Platinum-Coin.Name")));

        for (String l : Main.getInstance().getConfig().getStringList("Coins.Platinum-Coin.Lore")) {
            lore.add(Color.color(l));

        }

        m.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
        m.addItemFlags(ItemFlag.HIDE_ENCHANTS);

        m.setLore(lore);
        g.setAmount(amount);
        g.setItemMeta(m);

        return g;
    }
}
