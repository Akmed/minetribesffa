package me.povnu.mc;

import me.povnu.mc.Events.ShopInteract;
import me.povnu.mc.commands.Coins;
import me.povnu.mc.commands.FFA;
import me.povnu.mc.commands.Shop;
import me.povnu.mc.commands.Stats;
import me.povnu.mc.interfaces.ShopGUI;
import me.povnu.mc.utils.Cooldown;
import me.povnu.mc.utils.LocationsFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class Main extends JavaPlugin {

    private static Main instance;

    public Cooldown cooldown;

    private static LocationsFile locsFile = new LocationsFile();

    PluginManager pm = getServer().getPluginManager();

    public final HashMap<String, Integer> coins = new HashMap<>();


    public void onEnable() {
        instance = this;

        this.cooldown = new Cooldown();

        getCommand("ffa").setExecutor(new FFA(new LocationsFile()));
        getCommand("coins").setExecutor(new Coins());
        getCommand("shop").setExecutor(new Shop(new ShopGUI()));
        getCommand("stats").setExecutor(new Stats());
        locsFile.load();

        pm.registerEvents(new ShopInteract(), this);

        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    public static Main getInstance() {

        return instance;
    }
}
