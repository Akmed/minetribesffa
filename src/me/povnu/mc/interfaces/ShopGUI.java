package me.povnu.mc.interfaces;

import me.povnu.mc.utils.Color;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShopGUI {

    public ItemStack Border() {
        ItemStack b = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 3);
        ItemMeta m = b.getItemMeta();

        m.setDisplayName(Color.color("&8."));

        m.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
        m.addItemFlags(ItemFlag.HIDE_ENCHANTS);

        b.setItemMeta(m);

        return b;
    }

    public ItemStack Food() {

        ItemStack i = new ItemStack(Material.BREAD);

        ItemMeta m = i.getItemMeta();

        m.setDisplayName(Color.color("&e&lFood Shop"));

        List<String> lore = Arrays.asList(new String[]{"",
                ChatColor.GRAY + "You can purchase different types of",
                ChatColor.GRAY + "food from the food shop with your ",
                ChatColor.GRAY + "coins you have earned from killing and looting!",
                ""});

        m.setLore(lore);

        i.setItemMeta(m);

        return i;
    }

    public ItemStack Potions() {
        ItemStack i = new ItemStack(Material.POTION, 1, (byte) 16421);

        ItemMeta m = i.getItemMeta();

        m.setDisplayName(Color.color("&d&lPotion Shop"));

        List<String> lore = Arrays.asList(new String[]{"",
                ChatColor.GRAY + "You can purchase different types of",
                ChatColor.GRAY + "potions from the potion shop with your ",
                ChatColor.GRAY + "coins you have earned from killing and looting!",
                ""});

        m.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);

        m.setLore(lore);
        i.setItemMeta(m);

        return i;
    }

    public ItemStack Weapons() {
        ItemStack i = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta m = i.getItemMeta();

        m.setDisplayName(Color.color("&c&lWeapons Shop"));

        List<String> lore = Arrays.asList(new String[]{"",
                ChatColor.GRAY + "You can purchase different types of",
                ChatColor.GRAY + "Weapons from the Weapon shop with your ",
                ChatColor.GRAY + "coins you have earned from killing and looting!",
                ""});

        m.setLore(lore);
        i.setItemMeta(m);

        return i;
    }


    public ItemStack Armor() {
        ItemStack i = new ItemStack(Material.DIAMOND_CHESTPLATE);

        ItemMeta m = i.getItemMeta();

        m.setDisplayName(Color.color("&b&lArmor Shop"));

        List<String> lore = Arrays.asList(new String[]{"",
                ChatColor.GRAY + "You can purchase different types of",
                ChatColor.GRAY + "Armor from the Armor shop with your ",
                ChatColor.GRAY + "coins you have earned from killing and looting!",
                ""});

        m.setLore(lore);
        i.setItemMeta(m);

        return i;
    }

    public ItemStack Balance() {
        ItemStack i = new ItemStack(Material.DOUBLE_PLANT);

        ItemMeta m = i.getItemMeta();

        m.setDisplayName(Color.color("&6&lCoin Balance"));

        List<String> lore = Arrays.asList(new String[]{
                ChatColor.GRAY + "Your Coin balance is listed below.",
                "",
                ChatColor.YELLOW.toString() + ChatColor.BOLD + "Balance:" + " " + ChatColor.GREEN + "{amount}¢"
        });

        m.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
        m.addItemFlags(ItemFlag.HIDE_ENCHANTS);

        m.setLore(lore);
        i.setItemMeta(m);

        return i;
    }

    public ItemStack Exit() {
        ItemStack i = new ItemStack(Material.BARRIER);
        ItemMeta m = i.getItemMeta();

        m.setDisplayName(Color.color("&4&lExit"));

        m.addEnchant(Enchantment.ARROW_DAMAGE, 1, true);
        m.addItemFlags(ItemFlag.HIDE_ENCHANTS);

        i.setItemMeta(m);

        return i;
    }

    public void shopUI(Player p) {
        Inventory inv = Bukkit.createInventory(null, 45, Color.color("Coin Shop"));

        for (int i = 0; i < inv.getContents().length; i++) {
            if (((i >= 0) && (i <= 8)) || ((i >= 1) && (i <= 45)) || (i % 9 == 0) || ((i - 8) % 9 == 0)) {
                inv.setItem(i, this.Border());
            }

            inv.setItem(11, this.Food());
            inv.setItem(15, this.Potions());
            inv.setItem(29, this.Weapons());
            inv.setItem(33, this.Armor());
            inv.setItem(22, this.Balance());
            inv.setItem(40, this.Exit());

            p.openInventory(inv);

        }
    }
}
