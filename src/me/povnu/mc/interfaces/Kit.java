package me.povnu.mc.interfaces;

import me.povnu.mc.utils.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Kit {

    public static ItemStack chestPlate() {
        ItemStack i = new ItemStack(Material.CHAINMAIL_CHESTPLATE);

        i.setDurability((short) 190);

        return i;
    }

    public static void useKit(Player p) {
        p.getInventory().setChestplate(chestPlate());
        p.getInventory().addItem(new ItemStack(Material.WOOD_SWORD));
        p.getInventory().addItem(new ItemStack(Material.BREAD, 5));

        p.sendMessage(Color.color("&aYou have redeemed the starter kit."));

    }
}
