package me.povnu.mc.utils;

import org.bukkit.ChatColor;

public class Color {

    public static String color(String msg) {

        return ChatColor.translateAlternateColorCodes('&', msg);
    }

}
