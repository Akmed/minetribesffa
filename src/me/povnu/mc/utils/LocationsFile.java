package me.povnu.mc.utils;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class LocationsFile {

    public File locs = new File("plugins/MineTribesFFA/spawns.yml");
    public FileConfiguration l;

    public LocationsFile() {

        this.l = YamlConfiguration.loadConfiguration(this.locs);
    }

    public List<String> getLocations() {

        return this.l.getStringList("Spawn-Points");
    }

    public String spawnLocations(Location loc) {
        String locString = "";
        locString = "World:" + loc.getWorld().getName()
                + "\n" + "Coords:" +
                "\n" + "X:" + loc.getX() +
                "\n" + "Y:" + loc.getY() +
                "\n" + "Z:" + loc.getZ();

        return locString;
    }

    public boolean isSpawnLocation(Location loc) {
        String locs = spawnLocations(loc);
        List<String> locations = getLocations();

        return locations.contains(locs);
    }

    public void addSpawnLocation(Location loc) {
        String locs = spawnLocations(loc);
        List<String> locations = getLocations();
        locations.add(locs);
        this.l.set("Spawn-Points", locations);
        saveLocs();
    }

    public void removeSpawnLocation(Location loc) {
        String locs = spawnLocations(loc);
        List<String> locations = getLocations();

        if (locations.contains(locs)) {
            locations.remove(locs);
            this.l.set("Spawn-Points", locations);
            saveLocs();
        }
    }

    public void load() {
        if (!new File("plugins/MineTribesFFA").exists()) {
            new File("plugins/MineTribesFFA").mkdir();
        }
        if (!this.locs.exists()) {
            try {
                this.locs.createNewFile();
                this.l.set("Spawn-Points", "");
                saveLocs();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveLocs() {
        try {
            this.l.save(this.locs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadLocs() {
        this.l = YamlConfiguration.loadConfiguration(this.locs);
    }
}


