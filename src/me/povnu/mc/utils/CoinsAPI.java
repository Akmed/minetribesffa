package me.povnu.mc.utils;

import me.povnu.mc.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class CoinsAPI implements Listener {

    private static Main main;

    public CoinsAPI(Main main) {
        this.main = main;
    }

    public static Integer getCoins(String s) {
        if (isPlayer(s)) {
            return 0;
        }
        return main.coins.get(s);
    }

    public static void addCoins(String s, int coins) {
        Player p = Bukkit.getPlayer(s);
        int c = getCoins(s);

        c += coins;

        main.coins.put(s, c);
    }

    public static void removeCoins(String s, int coins) {
        int c = getCoins(s);

        c -= coins;

        main.coins.put(s, c);
    }

    public static void setCoins(String s, int coins) {
        main.coins.put(s, coins);
    }

    public static void addPlayer(String s) {
        if (isPlayer(s)) {
            main.coins.put(s, 0);
        }
    }

    public static boolean isPlayer(String p) {
        return !main.coins.containsKey(p);
    }
}
