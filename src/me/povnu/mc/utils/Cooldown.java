package me.povnu.mc.utils;

import java.util.HashMap;

import me.povnu.mc.Main;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;


public class Cooldown {

    private HashMap<Player, Integer> cooldown = new HashMap<>();

    public Integer getPlayer(Player player) {

        return (Integer) this.cooldown.get(player);
    }

    public boolean containsPlayer(Player player) {
        return this.cooldown.containsKey(player);
    }

    public void addPlayer(Player player, int seconds) {
        this.cooldown.put(player, Integer.valueOf(seconds));
    }

    public void removePlayer(Player player) {
        this.cooldown.remove(player);
    }

    public void runTimer(final Player player) {
        new BukkitRunnable() {
            public void run() {
                if (((Integer) Cooldown.this.cooldown.get(player)).intValue() > 0) {
                    Cooldown.this.cooldown.put(player,
                            Integer.valueOf(((Integer) Cooldown.this.cooldown.get(player)).intValue() - 1));
                }
                if (((Integer) Cooldown.this.cooldown.get(player)).intValue() == 0) {
                    Cooldown.this.cooldown.remove(player);

                    cancel();
                }
            }
        }.runTaskTimer(JavaPlugin.getPlugin(Main.class), 20L, 20L);
    }
}
